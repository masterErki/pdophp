<?php 
    
    /************************************************
    * Descripcion de la clase o plantilla.... Usuario
    *************************************************/

    class Usuario 
    {
        //propiedades
        private $nombre;
        private $apellidos;
        private $edad;

        //metodo constructor de la clase
        public function __construct($nom,$ape,$edad)
        {
            if(is_string($nom) and is_string($ape))
            {
                $this->nombre= $nom;
                $this->apellidos= $ape;
            }
            if(is_int($edad))
            {
                $this->edad= $edad;
            }
            else
            {
                $this->edad= 0;
            }
        }
        // Normalmente, se crea Medotos llamados GETTERS(obtenedores) y SETTERS(Establecedores)
        public function getNombre(){return $this->nombre;}
        
        public function setNombre($nom){ 
            if(is_string($nom)){
                if(strlen($nom) > 0){
                    $this->nombre= $nom;
                }
            }
        }

        public function getApellidos(){return $this->apellidos;}
        
        public function setApellidos($ape){ 
            if(is_string($ape)){
                if(strlen($ape)>0){
                    $this->apellidos = $ape;
                }
            }
        }

        public function getEdad(){return $this->edad;}
        
        public function setEdad($edad){ 
            if(is_int($edad)){
                if($edad>=0 and $edad <120){
                    $this->edad = $edad;
                }
            }
        }

        public function getIniciales()
        {       
            return strtoupper(substr($this->nombre,0,1).substr($this->apellidos,0,1));
        }


        //metodos de la clase
        public function dimeInfo()
        {
            $r = '';
            $r .= $this->apellidos;
            $r .= ' , ';
            $r .= $this->nombre;
            $r .= ' / ';
            $r .= $this->edad;
            $r .= ' años ';
            return $r;
        }
           

        public function cumplirAnnos()
        {
            $this->edad++;
        }

    }
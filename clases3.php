<?php 
    // **** ***** ***** INCLUYO LAS CLASES QUE VOY A UTILIZAR  ******* ******* *****
    require 'class.producto.php';
    require 'class.productos.php';
    require 'class.imagen.php';
    require 'phpqrcode/qrlib.php';

    $almcen = new Productos();
    $almcen->add(new Producto('altavoces',42.5));

    $p = new Producto('teclado bluetooth',15.5,7);
    $almcen->add($p);

    echo $almcen->dimeUnidades();
    echo $almcen->dimeValorStock();

    //var_dump($almcen->dimeInfo());

    echo "<hr>";

    //Accediendo a una propiedad statica
    echo Producto::$iva;

    //Cambiando valor a la propiedad estatica
    Producto::$iva = 18;
    echo "<br>";
    echo Producto::$iva;
    echo "<hr>";

    Producto::$iva = 21;
    echo "<br>";

    echo $almcen->productos[0]->dimeIva2();
    echo "<br>";
    echo $almcen->productos[1]->dimeIva2();

    echo "<hr>";

    echo 'El numero de productos creados es: '.Producto::$cantidad;
    echo "<br>";
    echo 'El numero de productos creados es: '.Producto::dimeCantidad();

    echo "<hr>\n";
    echo Imagen::pintaImagen('img/heineken.png');

    echo "<hr>\n";
    Imagen::$ancho=200;
    echo Imagen::pintaImagen('img/heineken.png');

    echo "<hr>\n";
    Imagen::$ancho=150;
    echo Imagen::pintaImagenConEnlace('img/heineken.png');


    // Utilizacion de la libreria para generar URL
    echo "<hr>\n";
    QRcode::png('http://google.es','img/qr-google.png','H');

?>

<img src="img/qr-google.png" width="250">





<?php 
     /***********************************************
    * Descripcion de la clase o plantilla.... Producto
    **************************************************/

    class Producto 
    {   
        // Propiedades o atributos de la clase
        protected $nombre; // solo puede acceder aquellas clases que tengan herencia
        public $precio;
        public $unidades;
        public static $iva = 21;
        public static $cantidad = 0 ;

        //Metodo constructor de la clase
        function __construct($nom,$pre,$uni=1)
        {
            $this->nombre = $nom;
            $this->precio = $pre;
            $this->unidades = $uni;
            self::$cantidad++;
        }

        // Metedos de la clase
        public function dimeInfo()
        {
            $r = '';
            $r .= $this->nombre;
            $r .= ' *** ';
            $r .= $this->precio;
            $r .= ' *** ';
            $r .= $this->unidades;
            $r .= ' *** ';
            return $r;
        }

        public function dimeIva($iva=21)
        {
            $ivaC = $this->precio*($iva/100);
            $ivaC = $ivaC * $this->unidades;
            return $ivaC;
        }

        public function dimeIva2()
        {
            // cuando tengamos que asignar una variable statica en vez de utilizar la palabra this ->> self

            $total = $this->precio * self::$iva /100;
            return $total;
        }

        public static function dimeCantidad()
        {
            return self::$cantidad;
        }

        // permite a una clase decidir cómo comportarse cuando se le trata como un string 
        public function __toString()
        {
            return 'El producto se llama: '.$this->nombre;
        }
    }
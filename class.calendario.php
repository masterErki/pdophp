<?php 

/**
* Calendario
*/
class Calendario
{
    private $diasSemana=array('domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado');
    private $meses=array('', 
        'enero', 
        'febrero', 
        'marzo', 
        'abril', 
        'mayo', 
        'junio', 
        'julio', 
        'agosto', 
        'septiembre', 
        'octubre', 
        'noviembre', 
        'diciembre'
    );
    
    //metodos de la clase
    function dimeFecha($fecha='ninguna'){
        //A partir de ahora, estas dos variables, pueden ser llamadas dentro
        //de la funcion, porque, se refiere a las de caracter global
        //global $diasSemana, $meses;
        //Si no recibo ninguna fecha, sera la actual con time()
        if($fecha=='ninguna'){
            $fecha=time();
        }
        //genero un resultado
        $resultado=$this->diasSemana[date('w',$fecha)].', '.date('d', $fecha).' de '.$this->meses[date('n',$fecha)].' de '.date('Y', $fecha);
        //devuelvo un resultado
        return $resultado;
    }
    function dimeMes($mes=0, $anyo=0){

        //global $diasSemana, $meses;

        if($mes==0){
            $mes=date('n');
        }
        if($anyo==0){
            $anyo=date('Y');
        }
        $resultado='<table><tr><td  align="center" colspan="7">'.$this->meses[$mes].' - '.$anyo.'</td></tr><tr><td  align="center">L</td><td  align="center">M</td><td  align="center">X</td><td  align="center">J</td><td  align="center">V</td><td  align="center">S</td><td  align="center">D</td></tr>';

        $fechaAuxiliar=mktime(0,0,0,$mes,1,$anyo);
        $dia=date('w', $fechaAuxiliar);

        //Pintamos los dias del calendario vacios, para empezar en el dia 
        //correcto de la semana
        if($dia!=1){
            $resultado.='<tr>';
            if($dia==0){
                $dia=7;
            }
            for($i=1;$i<$dia; $i++){
                $resultado.='<td align="center">&nbsp;</td>';
            }
        }

        $ultimoDiaMes=date('t', $fechaAuxiliar);
        for ($dia=1; $dia <= $ultimoDiaMes; $dia++) { 

            $f=mktime(0,0,0,$mes, $dia, $anyo);

            if(date('w', $f)==1){
                $resultado.='<tr>';
            }

            $resultado.='<td align="center">'.$dia.'</td>';

            if(date('w', $f)==0){
                $resultado.='</tr>';
            }

        }

        $resultado.='</table>';
        return $resultado;  
    }

    function dimeAnyo($anyo=0){
        if($anyo==0){
            $anyo=date('Y');
        }
        $resultado='<table border="1">';
        for($m=1;$m<=12;$m++){

            //$resultado.='<tr></tr>';

            if($m%4==1){
                $resultado.='<tr>';
            }

                $resultado.='<td valign="top">'.$this->dimeMes($m,$anyo).'</td>';

            if($m%4==0){
                $resultado.='</tr>';
            }

        }
        $resultado.='</table>';
        return $resultado;
    }
}
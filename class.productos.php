<?php 


    /**
    * Class PRODUCTOS:
    */

    class Productos
    {
        // Propiedades        /***************/
        public $productos;

        // Metodo constructor /***************/
        function __construct()
        {
            $this->productos = [];
        }

        //Metodos de la clase  /**************/
        public function add($val)
        {
            $this->productos[]=$val;
        }

        public function dimeUnidades()
        {
            $u = 0;
            foreach($this->productos as $p) 
            { 
                $u += $p->unidades;
            }
            return $u;
        }

        public function dimeValorStock()
        {
            $u = 0;
            foreach ($this->productos as $p) 
            {
                $u += ($p->unidades*$p->precio);
            }
            return $u;
        }



        public function dimeInfo()
        {
            return $this->productos;
        }
    }
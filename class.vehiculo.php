<?php 

    class Vehiculo extends Producto
    {
        public $marca;
        public $anno;

        public function __construct($nom, $pre,$mar,$ann,$uni=1)
        {
            parent::__construct($nom,$pre,$uni);
            $this->marca = $mar;
            $this->anno = $ann;

        }

        public function dimeInfo()
        {
            $result = parent::dimeInfo();
            $result.= ' / '.$this->marca;
            $result.= ' / '.$this->anno;
            return $result;

        }

        public function dimeNombre()
        {
            return $this->nombre;
        }
    }
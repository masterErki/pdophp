<?php 

class Contacto extends Persona
{
    private $email;
    
    public static $cantidad=0;

    public function __construct($nom,$apel,$tel,$em)
    {
        parent::__construct($nom,$apel,$tel); 
        $this->email = $em;
        self::$cantidad++;   
    } 


    public function dimeInfo()
    {
        $r='';
        $r.= parent::dimeInfo();
        $r.= ' -'.$this->email;
        return $r;
    }

}
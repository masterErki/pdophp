<?php 

    class Agenda
    {
      public $agenda;

      // Metodo constructor /***************/
      function __construct()
      {
          $this->agenda = [];
      }

      //Metodos de la clase  /**************/
      //add(Persona/Contacto/Usuario) 
      public function add($val)
      {
          $this->agenda[]=$val;
      }
      
      //devuelve el numero de Personas/Contactos/Usuarios 
      public function contar()
      {
        $u = 0;$p = 0;$c = 0;

        foreach($this->agenda as $age) 
        { 
          if(get_class($age )=='Persona')
            $p++;

          if(get_class($age )=='Contacto')
            $c++;

          if(get_class($age )=='Usuario')
            $u++;
        }
        return 'Personas: '.$p.' / Contactos: '.$c.' / Usuarios: '.$u;
      } 

      // tengo que cambiar las propiedades a visibles(public) o genero setter and getters 
      public function mostrar($nom)
      {
        foreach ($this->agenda as $pers) {
          if ($pers->getNombre() == $nom) {
            return $pers->dimeInfo();
          }
        }
        return 'No encontrado';
      }

      // funcion mostrarTipo solo muestra los tipos 
      public function mostrarTipo($clase)
      {
        $r = [];

        foreach ($this->agenda as $pers) 
        {
          if (get_class($pers) == $clase) {
            $r[]=$pers;
          }
        }
        return $r;
      }
    }
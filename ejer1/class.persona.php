<?php 
    
    /**
    * Persona
    */              
    class Persona
    {
        private $nombre;
        private $apellidos;
        private $telefono;
        public static $cantidad=0;

        
        public function __construct($nom='', $apel='', $tele='')
        {
            $this->apellidos=$apel;
            $this->nombre   =$nom;
            $this->telefono =$tele;
            self::$cantidad++;
        }

        public function dimeInfo()
        {
            $r='';
            $r.= ' / '.$this->getNombre();
            $r.= ' / '.$this->getApellidos();
            $r.= ' /'.$this->getTelefono();
            return $r;
        }

    
        /**
         * Gets the value of nombre.
         *
         * @return mixed
         */
        public function getNombre()
        {
            return $this->nombre;
        }

        /**
         * Sets the value of nombre.
         *
         * @param mixed $nombre the nombre
         *
         * @return self
         */
        private function _setNombre($nombre)
        {
            $this->nombre = $nombre;

            return $this;
        }

        /**
         * Gets the value of apellidos.
         *
         * @return mixed
         */
        public function getApellidos()
        {
            return $this->apellidos;
        }

        /**
         * Sets the value of apellidos.
         *
         * @param mixed $apellidos the apellidos
         *
         * @return self
         */
        private function _setApellidos($apellidos)
        {
            $this->apellidos = $apellidos;

            return $this;
        }

        /**
         * Gets the value of telefono.
         *
         * @return mixed
         */
        public function getTelefono()
        {
            return $this->telefono;
        }

        /**
         * Sets the value of telefono.
         *
         * @param mixed $telefono the telefono
         *
         * @return self
         */
        private function _setTelefono($telefono)
        {
            $this->telefono = $telefono;

            return $this;
        }

        /**
         * Gets the value of cantidad.
         *
         * @return mixed
         */
        public function getCantidad()
        {
            return $this->cantidad;
        }

        /**
         * Sets the value of cantidad.
         *
         * @param mixed $cantidad the cantidad
         *
         * @return self
         */
        public function _setCantidad($cantidad)
        {
            $this->cantidad = $cantidad;

            return $this;
        }
    }
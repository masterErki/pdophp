<?php 

    //llamo a todas las clases que voy a necesitar
    require 'class.persona.php';
    require 'class.contacto.php';
    require 'class.usuario.php';
    require 'class.agenda.php';



    $p = new Persona('Pedro','Lopez Bueno','699658975');

    echo $p->dimeInfo();

    echo '<hr>';
    
    $p2 = new Persona('Jaime','Pedriz Loquiz','699898965');
    $c2 = new Contacto('Juan','Bello Loto','669858875','jbl@correo.com');
    $u2 = new Usuario('Pepe','Bueno Lopez ','699658975','pbl@loko','1234');

    echo 'Persona: '.$p2->dimeInfo().'<br>';
    echo "Es de la clase: ".get_class($p2);
    echo "<br>";
    echo 'Contacto: '.$c2->dimeInfo().'<br>';
    echo "Es de la clase: ".get_class($c2);
    echo "<br>";
    echo 'Usuario: '.$u2->dimeInfo().'<br>';
    echo "Es de la clase: ".get_class($u2);
    echo "<br>";

    echo '<hr>';

    $age = new Agenda();

    $age->add(new Persona('Jaime','Pedriz Loquiz','699898965'));
    $age->add(new Persona('Jaime2','Pedriz Loquiz','699898965'));
    $age->add(new Persona('Jaime3','Pedriz Loquiz','699898965'));
    $age->add(new Contacto('Juan','Bello Loto','669858875','jbl@correo.com'));
    $age->add(new Usuario('Pepe','Bueno Lopez ','699658975','pbl@loko','1234'));

    echo '<pre>';
    print_r($age);
    echo '</pre>';

    echo '<strong>'.$age->contar().'</strong>';
    echo '<hr>';

    echo $age->mostrar('Pepe');

    echo '<hr>';
    echo is_a($age->agenda[0],'Persona');
    echo '<hr>';
    //print_r( $age->mostrarTipo('Persona'));
    $result = $age->mostrarTipo('Persona');
    foreach ($result as $e) {
        echo $e->dimeInfo().'<br>';
    }


    
    

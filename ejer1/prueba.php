<?php 

	class Persona2
    {
        private $nombre;
        private $apellidos;
        private $telefono;
        public static $cantidad=0;

        
        public function __construct($nom='', $apel='', $tele='')
        {
            $this->apellidos=$apel;
            $this->nombre   =$nom;
            $this->telefono =$tele;
            self::$cantidad++;
        }

        public function dimeInfo()
        {
            $r='';
            $r.= ' / '.$this->getNombre();
            $r.= ' / '.$this->getApellidos();
            $r.= ' /'.$this->getTelefono();
            return $r;
        }
        public function getNombre()
        {
            return $this->nombre;
        }
        public function getApellidos()
        {
            return $this->apellidos;
        }
        public function getTelefono()
        {
            return $this->telefono;
        }
    }
	/*******************************************/

    class Contacto2 
	{
	    public $email;
	    public $pers;
	    public static $cantidad=0;

	    public function __construct($em)
	    {
	        $this->pers = new Persona2(); 
	        $this->email = $em;
	        self::$cantidad++;   
	    } 


	    public function dimeInfo()
	    {
	        $r='';
	        $r.= $this->pers->dimeInfo();
	        $r.= ' -'.$this->email;
	        return $r;
	    }

	}
	/*******************************************/
	class Usuario2 
	{
	    public $login;
	    public $clave;
	    public $pers;
	    public static $cantidad=0;

	    public function __construct($log,$pass)
	    {
	        $this->pers = new Persona2(); 
	        $this->login = $log;   
	        $this->clave = $pass;
	        self::$cantidad++;   
	    }

	    public function dimeInfo()
	    {
	        $r='';
	        $r.= ''.$this->pers->dimeInfo();
	        $r.= ' -'.$this->login;
	        $r.= ' -'.$this->clave;
	        return $r;
	    }

	}
	/*******************************************/
	class Agenda2
    {
      public $agenda;
      // Metodo constructor /***************/
      function __construct()
      {
          $this->agenda = [];
      }

      //Metodos de la clase  /**************/
      //add(Persona/Contacto/Usuario) 
      public function add($val)
      {
          $this->agenda[]=$val;
      }
      
      //devuelve el numero de Personas/Contactos/Usuarios 
      public function contar()
      {
        $u = 0;$p = 0;$c = 0;

        foreach($this->agenda as $age) 
        { 
          if(get_class($age )=='Persona2')
            $p++;

          if(get_class($age )=='Contacto2')
            $c++;

          if(get_class($age )=='Usuario2')
            $u++;
        }
        return 'Personas: '.$p.' / Contactos: '.$c.' / Usuarios: '.$u;
      } 

      // tengo que cambiar las propiedades a visibles(public) o genero setter and getters 
      public function mostrar($nom)
      {
        foreach ($this->agenda as $pers) {
          	if(get_class($pers)== 'Persona2'){
	          if ($pers->getNombre() == $nom) {
	            return $pers->dimeInfo();
	          }
	       	}
	       	else{
	       		return $pers->pers->dimeInfo();
	       	}
        }
      } 
    }
    /*******************************************/

	$p = new Persona2('Pepe','Lopez Lopez','69696969');
	echo $p->dimeInfo().'<br>';
	
	$c = new Contacto2('pepe@correo.com');
	$c->pers = $p;
	echo $c->dimeInfo().'<br>';
	echo $c->pers->getNombre();

	$u = new Usuario2('pll#loko','asdf');
	$u->pers = $p;

	echo $u->dimeInfo();

	echo '<hr>';

	$ag = new Agenda2();

	$p2 = new Persona2('Marta','Leva Lote','69853969');
	$c2 = new Contacto2('marta@correo.com');
	$u2 = new Usuario2('marta','ñlkj');
	$c2->pers = $p2;

	$p3 = new Persona2('Juan','Ruiz Lopez','96969696');
	$c3 = new Contacto2('juan@correo.com');
	$u2 = new Usuario2('juan','1234');
	$c3->pers = $p3;

	$p4 = new Persona2('Eva','Lopez Ruiz','70707070');
	$c4 = new Contacto2('eva@correo.com');
	$u2 = new Usuario2('eva','0987');
	$c4->pers = $p4;

	$p5 = new Persona2('Raul','Tela Marinera','80808080');
	$c5 = new Contacto2('raul@correo.com');
	$u2 = new Usuario2('raul','as12');
	$c5->pers = $p5;

	$ag->add($p);
	$ag->add($p2);
	$ag->add($p3);

	$ag-> add($c4);
	$ag-> add($c5);

	$ag->add($u);

	echo '<pre>';
    print_r($ag);
    echo '</pre>';

    echo '<hr>';
    echo '<strong>'.$ag->contar().'</strong><br>';

    /*foreach ($ag as $el) {
    	echo get_class(is_object($el)).'<br>';
    }*/
	
	//echo get_class($ag);

	echo '<hr>';
	echo $ag->mostrar('Eva');


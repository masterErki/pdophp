<?php 

class Usuario extends Persona
{
    public $login;
    public $clave;
    public static $cantidad=0;

    public function __construct($nom,$apel,$tel,$log,$pass)
    {
        parent::__construct($nom,$apel,$tel); 
        $this->login = $log;   
        $this->clave = $pass;
        self::$cantidad++;   
    }

    public function dimeInfo()
    {
        $r='';
        $r.= ''.parent::dimeInfo();
        $r.= ' -'.$this->login;
        $r.= ' -'.$this->clave;
        return $r;
    }

}
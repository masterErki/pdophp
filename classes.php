<?php
    require 'class.productos.php';
    require 'class.usuario.php';

/**
 * Descripcion de la clase o plantilla....
 */

/**********************************************************************/
//Inicio de la class Producto
echo '<hr>';
echo '<h2>Clase Producto</h2>';
echo '<hr>';


/********************* Instanciacion de la clase */

$pro1 = new Producto();
$pro1->monbre = 'Teclado Mac';
$pro1->precio = 50.1;
$pro1->unidades = 1;

//llamo al metodo de la clase para obtener la infor de la clase
echo $pro1->dimeInfo();

echo '<br>';

$pro2 = new Producto();
$pro2->monbre = 'Teclado Window';
$pro2->precio = 70.2;
$pro2->unidades = 5;
echo $pro2->dimeInfo();


/**
 * Calse para crear usuarios
 */

echo '<hr>';
echo '<h2>Clase Usuario</h2>';
echo '<hr>';
  
$usu1 = new Usuario('Pedrito','Sin Locos',40);
//$usu1->edad = 70;   // obtenemos un error porque la propiedad es privada y no me deja acceder a ella
echo $usu1->dimeInfo().'<br>';
$usu1->cumplirAnnos();
echo $usu1->dimeInfo().'<br>';
echo $usu1->getNombre().'<br>';

$usu1->setNombre('Javier');
echo $usu1->getNombre().'<br>';

$usu1->setApellidos('Lokolia Lokolia');
echo $usu1->dimeInfo().'<br>';

echo $usu1->getIniciales().'<br>';

echo '<hr>';



$pro3 = new Producto();
$pro3->nombre = 'Raton bluetooth';
$pro3->unidades = 10;
$pro3->precio = 20;

echo $pro3->dimeIva().' Iva >>> 21%<br>'; //me devuelve el 21% iva
echo $pro3->dimeIva(18).' Iva >>> 18%<br>'; //me devuelve el 18% iva


?>





<?php 

    /**
    * 
    */
    class Imagen
    {
        public static $ancho =100;

        public static function pintaImagen($img)
        {
            
            return '<img src="'.$img.'" width="'.self::$ancho.'">';
        }

        public static function pintaImagenConEnlace($img)
        {
            $result = '<a href="'.$img.'">';
            $result .= self::pintaImagen($img);
            $result .= '</a>';
            return $result;

        }
        
    }
?>


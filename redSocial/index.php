<?php
  //Iniciamos la sesion
  session_start();

  //Realizamos los requieres
  require 'clases/class.conexion.php';
  require 'clases/class.form.php';
  require 'clases/class.menu.php';
  require 'clases/class.post.php';
  require 'clases/class.noticia.php';
  require 'clases/class.video.php';
  require 'clases/class.usuario.php';
  require 'clases/class.repositorio.php';
  require 'clases/class.posts.php';
  require 'clases/class.noticias.php';
  require 'clases/class.videos.php';
  require 'clases/class.usuarios.php'; 

  if(isset($_GET['p'])){
    $p = $_GET['p'];
  }else{
    $p = 'inicio.php';
  }



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Proyecto BootStrap 01</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Mi css -->
    <link rel="stylesheet" type="text/css" href="css/micss.css">

  </head>
  <body>
    <section class="container">
      <div class="row">
        <!-- header -->
        <header class="jumbotron" style="background: #E6D1D1; box-shadow: 2px 2px 4px #caadad;;">
          <h1 class="text-center" style="color: #840d0d; text-shadow: 2px 2px 4px white;">La mejor Social-Red@Social</h1>
        </header>

          
        <!-- iluimos el login -->
        <?php require 'includes/login.php'; ?>
             

        
          
        <!-- menu izquierdo -->
        <nav class="col-md-2 well" style="background: #DFE7F0;box-shadow: 2px 2px 4px #b5c9d6;"><?php require('includes/menui.php'); ?></nav>
      
        <!-- contenido general -->
        <section class="col-md-8" style="padding: 25px;"><?php require('paginas/'.$p); ?></section>
        
        <!-- menu derecho -->
        <nav class="col-md-2 well" style="background: #DFE7F0;box-shadow: 2px 2px 4px #b5c9d6;"><?php require('includes/menud.php'); ?></nav>

      </div>

    

    </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- editor del textarea -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
  </body>
</html>
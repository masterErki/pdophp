<?php 

    /**
    * Fichero clases/class.posts.php
    */

    class Posts extends Repositorio
    {
        
        //////////////////////////////////////////////////////////////////
        ///////// LISTADO
        /////////////////////////////////////////////////////////////////
        public function listado()
        {
            //
            $query = parent::listado();

            while($fila=$query->fetch_array()){

                $this->elementos[]= new Post($fila);
            }

            $r='';
            foreach ($this->elementos as $elem) {
                $r .= '<article>';
                $r .= $elem->getTitulo();

                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elem->getId().'">Ver</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elem->getId().'" onclick="if(!confirm(\'Estas seguro\')){return false;}">Borrar</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elem->getId().'">Modificar</a>';

                $r .= '</article>';

            }
            $r .= ' - <a href="index.php?p='.$this->fich.'&accion=insertar">Insertar</a>';
            return $r;
        }

        //////////////////////////////////////////////////////////////////
        ///////// VER
        /////////////////////////////////////////////////////////////////

        public function ver($id)
        {
            $fila=parent::ver($id);
            $post= new Post($fila);

            $r = ' - <a href="index.php?p='.$this->fich.'&accion=listado">Inicio</a>'."\n";
            $r .= '
                <article>
                <header><h3>'.$post->getTitulo().'</h3></header>
                <section class="well">'.$post->getContenido().'</section>
                <footer class="text-right">'.$post->getFecha().'</footer>
                </article>
                ';

            return $r;
            
        }

        

        //////////////////////////////////////////////////////////////////
        ///////// INSERTAR
        /////////////////////////////////////////////////////////////////

        public function insertar()
        {
            $r = Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'$accion=insercion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo','',['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::txt_area('comentarios','Comentarios','',[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce un comentario',
                'class'=>'form-control'
            ]);

            $r .= Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();


            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCION
        /////////////////////////////////////////////////////////////////

        public function insercion()
        {
            $tit = $_POST['titulo'];
            $cont = $_POST['contenido'];
            $fecha = Date('Y-m-d H:i:s');

            $sql = "INSERT INTO $this->tabla(titulo,contenido,fecha) VALUES ('$tit','$cont','$fecha')";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'Insercion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido insertar el registro';
            }
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICAR
        /////////////////////////////////////////////////////////////////

        public function modificar($id)
        {
            

            $fila=parent::modificar($id);
            $post= new Post($fila);

            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio');

            $r .= Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'$accion=modificacion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo',$post->getTitulo(),['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::txt_area('contenido','Comentarios',$post->getContenido(),[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce un comentario',
                'class'=>'form-control'
            ]);

            $r.= Form::input('hidden','id','',$_GET['id']);
            $r .= Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();
            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICACION
        /////////////////////////////////////////////////////////////////

        public function modificacion()
        {
           
            $id = $_POST['id'];
            $tit = $_POST['titulo'];
            $cont = $_POST['contenido'];



            $sql = "UPDATE $this->tabla SET titulo='$tit',contenido='$cont' WHERE id=$id";
            echo $sql;

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'modificacion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido modificar el registro';
            }
            
        }

      

    }


<?php 

    

    class Form
    {
        public static function ini_form($atributos)
        {
            $att=null;
            foreach ($atributos as $key => $val) {
                $att .= " $key='$val'";
            }
            return "<form$att>\n";
        }

        public static function fnal_form()
        {
            return "</form>\n";
        }

        public static function input($type, $name,$content,$value,$ops=[]){
            $op=null;
            foreach ($ops as $key => $val) {
                $op .= " $key='$val'";
            }

            $r = "";
            $r .= "<div class='form-group'>\n";
            $r .= "\t<label for='$name' control-label>$content</label>\n";
            $r .= "\t<input type='$type' id='$name' name='$name' value='$value'$op>\n";
            $r .= "</div>\n";
            return $r;
        }

        public static function txt_area($name,$content,$txt_cont='',$optional=[]){
            $op=null;
            foreach ($optional as $key => $val) {
                $op .= " $key='$val'";
            }
            $r = "";
            $r .= "<div class='form-group'>\n";
            $r .= "\t<label for='$name' control-label>$content</label>\n";
            $r .= "\t<textarea id='$name' name='$name'$op>$txt_cont</textarea>\n";
            $r .= "</div>\n";
            return $r;
        }

        static function btn_HTML5($type,$content,$atributos=[])
        {
            $att=null;
            foreach ($atributos as $key => $val) {
                $att .= " $key='$val'";
            }

            return "<button type='$type'$att>$content</button>\n";
        }

        static function a($href,$content,$optional=[])
        {
            $op = null;
            foreach ($optional as $key => $val) {
                $op.= " $key='$val'";
            }
            return "<a href='$href'".$op.">$content</a>\n";
        }

            

    }



 ?>


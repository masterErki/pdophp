<?php 

    class Menu{
        //propiedades de la clase
        private $enlaces;
        private $nombres;
        private $alineacion;
        private $tit;

        //metodo constructor
        public function __construct($enl,$nom)
        {
            $this->enlaces = $enl;
            $this->nombres  = $nom;
            $this->alineacion = 'text-left';
            $this->tit = '';
        }

        public function pintar()
        {
            global $p;

            $r =$this->getTitulo();
            $r .= '<ul class="nav nav-pills nav-stacked">';
            for ($i=0; $i < count($this->enlaces); $i++) 
            { 
                if($p == $this->enlaces[$i]){
                    $c = 'active';
                }
                else{
                    $c = '';
                }

                $r .='<li class="'.$this->alineacion.' '.$c.'"><a href="index.php?p='.$this->enlaces[$i].'">'.$this->nombres[$i].'</a></li>';
            }
            $r.='</ul>';
            return $r;
        }

        public function setAlineacion($ali)
        {
                switch ($ali) {
                    case 'derecha':
                        $this->alineacion = 'text-right';
                        break;
                    case 'centro':
                        $this->alineacion = 'text-center';
                        break;
                    
                    default:
                        $this->alineacion = 'text-left';
                        break;
                }
        }

        public function setTitulo($titu)
        {
            if($titu != ''){
                $this->tit = '<h4>'.$titu.'</h4>';
            }
            
        }
            

        public function getTitulo()
        {
            return $this->tit;
        }

    }
?>


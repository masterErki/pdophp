<?php 

    class Noticias extends Repositorio
    {
        //////////////////////////////////////////////////////////////////
        ///////// LISTADO
        /////////////////////////////////////////////////////////////////
        public function listado()
        {
            
            $query = parent::listado();

            while($fila=$query->fetch_array()){

                $this->elementos[]= new Noticia($fila);
            }

            $r='';
            foreach ($this->elementos as $elem) {
                $r .= '<article> <header><h2>';
                $r .= $elem->getTitulo();
                $r .= '<small>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elem->getId().'">Ver</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elem->getId().'" onclick="if(!confirm(\'Estas seguro\')){return false;}">Borrar</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elem->getId().'">Modificar</a>';

                $r .= '</small></h2><section class="well" style="background: #f9f2ec;">';
                
                $r .= '<footer style="color:red;font-style:italic;">'.$elem->getFecha().'</footer>';
                $r .= '</article>';

            }
            $r .= '<a href="index.php?p='.$this->fich.'&accion=insertar">'.Form::btn_HTML5('button','Insertar',['class'=>'btn btn-primary']).'</a>';
            return $r;
        }

        //////////////////////////////////////////////////////////////////
        ///////// VER
        /////////////////////////////////////////////////////////////////

        public function ver($id)
        {
            $fila=parent::ver($id);
            $noticia= new Noticia($fila);

            $r = ' - <a href="index.php?p='.$this->fich.'&accion=listado">Inicio</a>'."\n";
            $r .= '
                <article>
                <header><h3>'.$noticia->getTitulo().'</h3></header>
                <section class="well">'.$noticia->getContenido().'</section>
                <footer class="text-right">'.$noticia->getAutor().' - '.$noticia->getFecha().'</footer>
                </article>
                ';

            return $r;
            
        }

        

        //////////////////////////////////////////////////////////////////
        ///////// INSERTAR
        /////////////////////////////////////////////////////////////////

        public function insertar()
        {
            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio');
            $r.= Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'&accion=insercion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo','',['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::txt_area('comentarios','Comentarios','',[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce un comentario',
                'class'=>'form-control'
            ]);
            $r.= Form::input('text','autor','Autor','',['class'=>'form-control','placeholder'=>'Introduce el autor']);

            $r .= Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();


            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCION
        /////////////////////////////////////////////////////////////////

        public function insercion()
        {
            $tit = $_POST['titulo'];
            $aut = $_POST['autor'];
            $cont = $_POST['contenido'];
            $fecha = Date('Y-m-d H:i:s');

            $sql = "INSERT INTO $this->tabla(titulo,autor,contenido,fecha) VALUES ('$tit','$aut','$cont','$fecha')";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'Insercion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido insertar el registro';
            }
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICAR
        /////////////////////////////////////////////////////////////////

        public function modificar($id)
        {
            

            $fila=parent::modificar($id);
            $noticia= new Noticia($fila);

            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio');

            $r .= Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'&accion=modificacion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo',$noticia->getTitulo(),['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::input('text','autor','Autor',$noticia->getAutor(),['class'=>'form-control','placeholder'=>'Introduce el autor']);
            $r.= Form::txt_area('contenido','Comentarios',$noticia->getContenido(),[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce un comentario',
                'class'=>'form-control'
            ]);

            $r.= Form::input('hidden','id','',$_GET['id']);
            $r .= Form::btn_HTML5('submit','Modificar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();
            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICACION
        /////////////////////////////////////////////////////////////////

        public function modificacion()
        {
           
            $id = $_POST['id'];
            $tit = $_POST['titulo'];
            $aut = $_POST['autor'];
            $cont = $_POST['contenido'];



            $sql = "UPDATE $this->tabla SET titulo='$tit',autor='$aut',contenido='$cont' WHERE id=$id";
            echo $sql;

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'modificacion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido modificar el registro';
            }
            
        }   



    }
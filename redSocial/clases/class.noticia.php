<?php 

    class Noticia 
    {
        //propiedades
        private $id;
        private $titulo;
        private $autor;
        private $contenido;
        private $fecha;

        function __construct($fila)
        {
            $this->id  = $fila['id'];
            $this->titulo  = $fila['titulo'];
            $this->autor  = $fila['autor'];
            $this->contenido = $fila['contenido'];
            $this->fecha = $fila['fecha'];
        }

        public function getId() {return $this->id; }
        public function getTitulo() {return $this->titulo; }
        public function getAutor() {return $this->autor; }
        public function getContenido() {return $this->contenido; }
        public function getFecha() {return $this->fecha; }

        public function set_Titulo($tit) { $this->titulo = $tit; }
    }
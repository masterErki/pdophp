<?php 

    /**
    * Fichero clases/class.posts.php
    */

    class Videos extends Repositorio
    {
        
        //////////////////////////////////////////////////////////////////
        ///////// LISTADO
        /////////////////////////////////////////////////////////////////
        public function listado()
        {
            //
            $query = parent::listado();

            while($fila=$query->fetch_array()){

                $this->elementos[]= new Video($fila);
            }

            $r='';
            foreach ($this->elementos as $elem) {
                $r .= '<article> <header><h2>';
                $r .= $elem->getTitulo();
                $r .= '<small>';

                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elem->getId().'">Ver</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elem->getId().'" onclick="if(!confirm(\'Estas seguro\')){return false;}">Borrar</a>';
                $r .= ' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elem->getId().'">Modificar</a>';

                $r .= '</small></h2><section class="well" style="background: #f9f2ec;">';
                $r .= '<iframe width="560" height="315" src="'.$elem->getEnlace().'" frameborder="0"></iframe>';
                $r .= '<footer style="color:red;font-style:italic;">'.$elem->getFecha().'</footer>';
                $r .= '</article>';

            }
            $r .= '<a href="index.php?p='.$this->fich.'&accion=insertar">'.Form::btn_HTML5('button','Insertar',['class'=>'btn btn-primary']).'</a>';
            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// VER
        /////////////////////////////////////////////////////////////////

        public function ver($id)
        {
            $fila=parent::ver($id);
            $video= new Video($fila);

            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio')."\n";
            $r .= '
                <article>
                <header><h3>'.$video->getTitulo().'</h3></header>
                <section class="well">
                <iframe width="560" height="315" src="'.$video->getEnlace().'" frameborder="0"></iframe>
                
                </section>
                <footer class="text-right">'.$video->getAutor().' - '.$video->getFecha().'</footer>
                </article>
                ';

            return $r;
            
        }

        

        //////////////////////////////////////////////////////////////////
        ///////// INSERTAR
        /////////////////////////////////////////////////////////////////

        public function insertar()
        {
            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio');
            $r .= Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'$accion=insercion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo','',['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::input('text','enlace','Enlace','',['class'=>'form-control','placeholder'=>'Introduce la URL']);
            $r.= Form::input('text','autor','Autor','',['class'=>'form-control','placeholder'=>'Introduce el autor']);
            

            $r .= Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();


            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCION
        /////////////////////////////////////////////////////////////////

        public function insercion()
        {
            $tit = $_POST['titulo'];
            $aut = $_POST['autor'];
            $url = $_POST['enlace'];
            $fecha = Date('Y-m-d H:i:s');


            $sql = "INSERT INTO $this->tabla(titulo,enlace,autor,fecha) VALUES ('$tit','$url','$aut','$fecha')";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'Insercion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido insertar el registro';
            }
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICAR
        /////////////////////////////////////////////////////////////////

        public function modificar($id)
        {
            

            $fila=parent::modificar($id);
            $video= new Video($fila);

            $r = Form::a('index.php?p='.$this->fich.'&accion=listado','Inicio');  // me lleva a la pagina de inicio

            $r .= Form::ini_form([
                'action'=>'index.php?p='.$this->fich.'$accion=modificacion',
                'method'=>'POST',
                'role'=>'form',
                'class' =>'form-horizontal'
            ]);

            $r.= Form::input('text','titulo','Titulo',$video->getTitulo(),['class'=>'form-control','placeholder'=>'Introduce tu titulo']);
            $r.= Form::input('text','enlace','Enlace',$video->getEnlace(),['class'=>'form-control','placeholder'=>'Introduce la URL']);
            $r.= Form::input('text','autor','Autor',$video->getAutor(),['class'=>'form-control','placeholder'=>'Introduce el autor']);

            $r.= Form::input('hidden','id','',$_GET['id']);
            $r .= Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary']);


            $r.= Form::fnal_form();
            return $r;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICACION
        /////////////////////////////////////////////////////////////////

        public function modificacion()
        {
           
            $id = $_POST['id'];
            $tit = $_POST['titulo'];
            $aut = $_POST['autor'];
            $url = $_POST['enlace'];



            $sql = "UPDATE $this->tabla SET titulo='$tit',enlace='$url',autor='$aut' WHERE id=$id";
            //echo $sql;

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'modificacion con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido modificar el registro';
            }
            
        }

      

    }


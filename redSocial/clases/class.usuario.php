<?php 

    class Usuario 
    {
        //propiedades
        private $id;
        private $nombre;
        private $correo;
        private $password;
        private $estado;
        private $fechaAlta;
        private $sesion;

        function __construct($fila)
        {
            $this->id  = $fila['id'];
            $this->nombre  = $fila['nombre'];
            $this->correo  = $fila['email'];
            $this->password = $fila['password'];
            $this->estado = $fila['estado'];
            $this->fecha = $fila['fechaAlta'];
            $this->sesion = $fila['sesion'];
        }

        public function getId() {return $this->id; }
        public function getNombre() {return $this->nombre; }
        public function getCorreo() {return $this->correo; }
        public function getpassword() {return $this->password; }
        public function getFecha() {return $this->fecha;}
        public function getSesion() {return $this->sesion;}
        public function getEstado() {return $this->estado;}

        public function set_Nombre($tit) { $this->nombre = $tit; }
        public function set_Estado($tit) { $this->estado = $tit; }
    }
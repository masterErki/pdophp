<?php 

    /**
    * Fichero clases/class.posts.php
    */

    class Repositorio
    {
        protected $conn;
        protected $elementos;
        protected $tabla;
        protected $fich;
        
        public function __construct($tabla,$fich)
        {
            $this->conn = Conexion::$conn;
            $this->elementos = [];
            $this->fich = $fich;
            $this->tabla = $tabla;

        }

        public function rellenar()
        {
        }

        //////////////////////////////////////////////////////////////////
        ///////// LISTADO
        /////////////////////////////////////////////////////////////////
        public function listado()
        {
            $sql = "SELECT * FROM $this->tabla ORDER BY id DESC";

            //echo $sql;

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            return $query;
        }

        //////////////////////////////////////////////////////////////////
        ///////// VER
        /////////////////////////////////////////////////////////////////

        public function ver($id)
        {
            $sql = "SELECT * FROM $this->tabla WHERE id=$id";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            $fila=$query->fetch_assoc();
           
            return $fila;
        }

        //////////////////////////////////////////////////////////////////
        ///////// BORRAR
        /////////////////////////////////////////////////////////////////

        public function borrar($id)
        {
            $sql = "DELETE FROM $this->tabla WHERE id= $id";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                // return 'Borrado con exito';
                header('location:index.php?p='.$this->fich.'&accion=listado');
            }
            else{
                return 'Error: No se ha podido borrar el registro';
            }
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERTAR
        /////////////////////////////////////////////////////////////////

        public function insertar()
        {
            
            //se encargara su hija
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCION
        /////////////////////////////////////////////////////////////////

        public function insercion()
        {
            //se encargara su hija
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICAR
        /////////////////////////////////////////////////////////////////

        public function modificar($id)
        {
            $sql = "SELECT * FROM $this->tabla WHERE id=$id";

            // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            $fila=$query->fetch_assoc();
            
            return $fila;
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// MODIFICACION
        /////////////////////////////////////////////////////////////////

        public function modificacion()
        {
           
            //se encargará su hija
            
        }

        //////////////////////////////////////////////////////////////////
        ///////// ACCIONES
        /////////////////////////////////////////////////////////////////

        public function acciones()
        {
            //recojo la accion pasada por el usuario
            if(isset($_GET['accion'])){
                $accion = $_GET['accion'];
            }
            else{
                $accion = 'listado';
            }

            //realizo un switch por las diferentes acciones
            switch ($accion) {
                case 'ver':
                    $id =$_GET['id'];
                    return $this->ver($id);
                    break;

                case 'insertar':
                    return $this->insertar();
                    break;

                case 'insercion':
                    
                    return $this->insercion();
                    break;

                case 'modificar':
                    $id =$_GET['id'];
                    return $this->modificar($id);
                    break;

                case 'modificacion':
                    
                    return $this-> modificacion();
                    break;

                case 'borrar':
                    $id =$_GET['id'];
                    return $this->borrar($id);
                    break;

                case 'listado':
                default:
                    return $this->listado();
                    break;

            }
        }

    }


<?php 

/**
* Fichero clases/class.post.php
*/
class Post 
{
    //propiedades
    private $id;
    private $titulo;
    private $contenido;
    private $fecha;

    function __construct($fila)
    {
        $this->id  = $fila['id'];
        $this->titulo  = $fila['titulo'];
        $this->contenido = $fila['contenido'];
        $this->fecha = $fila['fecha'];
    }

    public function getId() {return $this->id; }
    public function getTitulo() {return $this->titulo; }
    public function getContenido() {return $this->contenido; }
    public function getFecha() {return $this->fecha; }

    public function set_Titulo($tit) { $this->titulo = $tit; }
}
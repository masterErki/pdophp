<?php 

/**
* Fichero clases/class.post.php
*/
class Video 
{
    //propiedades
    private $id;
    private $titulo;
    private $enlace;
    private $autor;
    private $fecha;

    function __construct($fila)
    {
        $this->id  = $fila['id'];
        $this->titulo  = $fila['titulo'];
        $this->enlace = $fila['enlace'];
        $this->autor = $fila['autor'];
        $this->fecha = $fila['fecha'];
    }

    public function getId() {return $this->id; }
    public function getTitulo() {return $this->titulo; }
    public function getAutor() {return $this->autor; }
    public function getEnlace() {return $this->enlace; }
    public function getFecha() {return $this->fecha; }

    public function set_Titulo($tit) { $this->titulo = $tit; }
}
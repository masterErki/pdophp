<?php 
    $msg = null;
    $users = new Usuarios('usuarios',$p);
    echo $users->acciones();

    if(isset($_POST['registrar'])){
        //Partimos de datos que sean correctos
        $datosCorrectos = true;

        if ($_POST['pass'] !== $_POST['pass2']) {
            $datosCorrectos = false;
            $msg = "Las contraseñas, no cooinciden";
        }
        else{
            $users->insercion();
        }

    }
?>
<div class="row  ">
    <h2>
        Registro de Usuario -
        <small>Registrate en mi web</small> 
        -
        <small>
            <a href="index.php">Cancelar</a>
        </small> 

    </h2>
    <div class="col-md-8 loginmodal-container text-center " style="margin-top: 50px;">

        <form action="index.php?p=registro.php" method="POST" name="frmLogin2" class="form-horizontal" role="form">
            
            <div class="form-group">
              <input type="text" name="nombre" placeholder="Escribe tu nombre">
            </div>

            <div class="form-group">
              <input type="email" name="email" placeholder="Escribe tu correo">
            </div>

            <div class="form-group">
              <input type="password" name="pass" placeholder="Escribe tu contraseña">
            </div>
            <div class="form-group">
              <input type="password" name="pass2" placeholder="Escribe tu contraseña">
            </div>

            <div class="form-group text-center">
              <input type="submit" name="registrar" class="login loginmodal-submit" value="Registrar">
            </div>

        </form>

        <h4><?php echo $msg; ?></h4>
    </div>
</div>
<?php 
    //fichero includes/menui.php

    $enlaces = ['inicio.php','usuarios.php','noticias.php','videos.php','contacto.php','comentario.php'];
    $nombres = ['Inicio','Usuarios','Noticias','Videos','Contactanos','Ultimos Comentarios'];

    $menui = new Menu($enlaces,$nombres);
    $menui->setAlineacion('izquierda');
    $menui->setTitulo('Menu Principal');
    echo $menui->pintar();

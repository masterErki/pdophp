<?php 
  
    //Si fuera necesario ..... declaro la variable de sesion ========================
    if(!isset($_SESSION['conectado'])){
      //Si hay ina cookie, conectamos al usuario
      if(isset($_COOKIE['correoUsu']))
      {
        $_SESSION['conectado']=true;
      }
      else
        $_SESSION['conectado']=null;
    }


    if(!isset($_SESSION['usuario'])){
         //Si hay una cookie, conectamos al usuario
        if(isset($_COOKIE['correoUsu']))
        {
          $correoUsu=$_COOKIE['correoUsu'];
          $sesionUsu=$_COOKIE['sesionUsu'];
          $conn = Conexion::$conn;

          $sql ="SELECT * FROM usuarios WHERE correo='$correoUsu' AND sesion='$sesionUsu'";
          $result = $conn->query($sql);
          $fila = $result->fetch_assoc();
          $_SESSION['usuario']=$fila;
        }
        else
          $_SESSION['usuario']=null;
    }



    //Posibilidad de que el usuario se conecte  =====================================
    if(isset($_POST['login'])){
        $email = $_POST['email'];
        $password = md5($_POST['pass']);

        $conn = Conexion::$conn;

        $sql="SELECT * FROM usuarios WHERE correo='$email' AND password='$password'";

        //echo $sql;

        $result = $conn->query($sql);

        //comprobamos que el email y correo sean los correctos
        if($result){
          
          if($fila = $result->fetch_assoc()){

            $_SESSION['conectado']=true;
            $_SESSION['usuario']=$fila;

            //si hemos marcado el checkbox, CREAMOS la COOKIE
            if(isset($_POST['mantener'])){
              setcookie('correoUsu',$email,time()+(60*60));
              $sesionUsu = session_id();
              setcookie('sesionUsu',session_id(),time()+(60*60));
              $sql ="UPDATE usuarios SET sesion='$sesionUsu' WHERE correo='$email'";
              $conn->query($sql);
            }

          }
        }

        // devuelve el id de la sesion
        //echo session_id();
        

        // if ($email == 'micorreo@correo.com' AND $password == '1234') {
        //     $_SESSION['conectado']=true;
        // }
    }
    //Posibilidad de que el usuario se desconecte ====================================
    if(isset($_GET['desconectar'])){
      
       // quitamos el sesionUsu de BBDD
      if(isset($_COOKIE['sesionUsu']))
      {
        $sesionUsu = $_COOKIE['sesionUsu'];
        $sql = "UPATE usuarios SET sesion='' WHERE sesion='$sesionUsu'";
        $conn->query($sql);
      }
        
      $_SESSION['conectado']=null;
      $_SESSION['usuario']=null;

      setcookie('correoUsu','',0);
      setcookie('sesionUsu','',0);
      setcookie('PHPSESSID','',-1);

    }
    

   //Posibilidad de comprobar si estamos o no conectados ============================
    if($_SESSION['conectado']){
        //echo $_SESSION['conectado'];
    ?>
      <div class="row">
        <div class="col-md-7"></div>
        <div class="col-md-4" style="background: rgba(245, 226, 199, 0.5); margin: -15px 0px 15px 85px; border: 1px dotted; border-radius: 10px;"> <h3>Bienvenido:  <?php echo $_SESSION['usuario']['nombre']; ?>
          <a href="index.php?desconectar=si">
          <?php echo Form::btn_HTML5('button','Desconectar',['class'=>'btn btn-info','style'=>'margin-bottom: 15px;margin-left:30px;']) ?>
          </a></h3>
        </div>
      </div>
    <?php }
    else{
?>

  <!-- Modal de login -->
  <div class="row text-right">
    <div class="col-md-12">
      <a href="#" data-toggle="modal" data-target="#login-modal">
        <button type="button" class="btn btn-info" style="margin-bottom: 15px;">Iniciar Sesión</button>
      </a>
      

        
      <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
        
          <div class="loginmodal-container">
            <h1>Logueate</h1><br>

            <form action="index.php" method="POST" name="frmLogin">
              <div class="form-group">
                <input type="email" name="email" placeholder="Pon tu correo">
              </div>
              <div class="form-group">
                <input type="password" name="pass" placeholder="Password">
              </div>
              <div class="form-group">
                <input type="checkbox" name="mantener" > No cerrar session
              </div>
              <div class="form-group">
                <input type="submit" name="login" class="login loginmodal-submit" value="Login">
              </div>
            </form>
            
            <div class="login-help">
              <a href="index.php?p=registro.php">Registro</a>
            </div>

          </div>
        </div>
          
      </div>
      
    </div>
  </div>
<?php } ?>
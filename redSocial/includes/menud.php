<?php 
    //fichero includes/menud.php

    $enlaces = ['donde.php','copyright.php','politica.php'];
    $nombres = ['Donde estamos','Copyright','Politica de privacidad'];

    $menud= new Menu($enlaces,$nombres);
    $menud->setAlineacion('derecha');
    //$menud->setTitulo('Menu Principal');

    echo $menud->pintar();
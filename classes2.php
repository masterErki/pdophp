<?php
    // Incluyo las clases que necesito 
    require 'class.productos.php';
    require 'class.usuario.php';
    require 'class.calendario.php';


    $usuarios = [];

    $u = new Usuario('Doloy','Lepo Tepo', 45);
    $usuarios[]=$u;

    $usuarios[]= new Usuario('Armando','Bronca Segura', 18);
    $usuarios[]= new Usuario('Eva','Leprilla Tajadao', 25);
     
    foreach ($usuarios as $user) {
        echo $user->getNombre().'<br>';
    }

    echo '<hr>';

    $cal = new Calendario();

    echo $cal->dimeMes();

    echo '<hr>';
    echo $cal->dimeAnyo();